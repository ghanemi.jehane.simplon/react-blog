import axios from "axios";

export class AuthService {

    static async login(credentials) {
        const response = await axios.post('https://blog-projet1.herokuapp.com/api/blog/user/login', credentials);
        localStorage.setItem('token', response.data.token);
        console.log(response)
        return response.data.user;
    }

    static async register(user) {
        const response = await axios.post('https://blog-projet1.herokuapp.com/api/blog/user', user);

        return response.data;
    }

    static async fetchUsers() {
        const response = await axios.get('https://blog-projet1.herokuapp.com/api/blog/user');
        return response.data;
    }

    static async fetchAccount(token){
        const response = await axios.get('https://blog-projet1.herokuapp.com/api/blog/user/account', {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        });
        return response.data;
    }
    static async fetchPosts() {
        const response = await axios.get('https://blog-projet1.herokuapp.com/api/blog/article');
        return response.data;
    }

    static async addPost(article) {
        const response = await axios.post('https://blog-projet1.herokuapp.com/api/blog/article/', article);
        return response.data;
    }



}