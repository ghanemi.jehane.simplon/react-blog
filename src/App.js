import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {Route, Switch } from 'react-router-dom';
import './App.css';
import { PostList } from './components/PostList';
import { Home } from './pages/Home';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { AddPostPage } from './pages/AddPostPage';
import NavBar from './components/Navbar';
import { ProtectedRoute } from './components/ProtectedRoute';
import { loginWithToken } from './stores/auth-slice';


function App() {
  const dispatch = useDispatch();


  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);

  return (
    <div>
      
      <NavBar/>
      <Switch>
        
        <Route exact path='/'>
          <Home/>
        </Route>

        <Route path="/login">
            <Login/>
          </Route>

          <Route path="/register">
            <Register />
          </Route>

          <Route exact path='/article'>
          <PostList/>
          </Route>

          <ProtectedRoute exact path ='/addpost'>
            <AddPostPage/>
          </ProtectedRoute>
      </Switch>
     
    </div>
  );
}

export default App;