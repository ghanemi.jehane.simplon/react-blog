import { useDispatch } from "react-redux";
import { AddPostForm } from "../components/AddPostForm";
import {addPosts} from "../stores/post-slice"

export function AddPostPage(){
    const dispatch = useDispatch();

    const addPost = (values) => {
        dispatch(addPosts(values))
    }

    return(
        <div>
            <AddPostForm onFormSubmit={addPost}/>
        </div>
    )
}
