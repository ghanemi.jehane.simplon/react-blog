import { useState } from "react";
import { useDispatch } from "react-redux"
import { AuthService } from "../services/AuthService";
import { login } from "../stores/auth-slice";
import { LoginForm } from "../components/LoginForm";
import { Card } from "@material-ui/core";


export function Login() {
    const dispatch = useDispatch();
    const [feedback, setFeedback] = useState('');
    const loginRequest = async (credentials) => {
        try {
            const user = await AuthService.login(credentials);
            dispatch(login(user));

            setFeedback('Hello ' + user.email + '!')
        } catch (error) {
            setFeedback('Credentials error')

        }
    }
    return (
        <>

        <Card>
            {feedback}
            <LoginForm onFormSubmit={loginRequest} />
        </Card>
        </>
    )
}