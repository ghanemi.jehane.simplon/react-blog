import { Card } from "@material-ui/core";
import { useState } from "react";
import { useDispatch } from "react-redux";
import RegisterForm from "../components/RegisterForm";
import { register } from "../stores/auth-slice";
export function Register() {
    const dispatch = useDispatch();
    const [feedback, setFeedback] = useState('');
    const registerRequest = async (data) => {
        try {

            dispatch(register(data));

            setFeedback('Hello' + data.email)
        } catch (error) {
            setFeedback('Credentials error')

        }
    }

    return (
        <>

        <Card>
            {feedback}
            <RegisterForm onFormSubmit={registerRequest} />
        </Card>
        </>
    )
}