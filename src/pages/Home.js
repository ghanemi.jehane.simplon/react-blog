import { useEffect } from "react";
import { useDispatch } from "react-redux";
import {CarouselComp } from "../components/Carousel";
import { PostList } from "../components/PostList";
import { fetchPosts } from "../stores/post-slice";

export function Home() {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);


    return (
        <div>
            <CarouselComp/>
            <PostList />
        </div>
    )
}