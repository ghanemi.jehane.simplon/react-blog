import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom";
import { logout } from "../stores/auth-slice";


export default function NavBar() {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.user);
  return (
    <div className="nav-wrapper">
      

      <nav className= "black">
          <a href="/" className="brand-logo red accent-4">Blog</a>

            {!user ? 
            <ul className="right hide-on-med-and-down">
            <Link to="/login">Login</Link>
             <Link to="/register">Register</Link>
            </ul>
         
          :
              <ul className="right hide-on-med-and-down">
                 <Link to="/addpost"><i className="material-icons">add</i></Link>
            <li onClick={() => dispatch(logout())}>Logout</li>
          </ul>
           }
      </nav>
    </div>



    
  )
}

