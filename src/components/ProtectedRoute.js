
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";


export function ProtectedRoute({ children, ...rest }) {
    const post = useSelector(state => state.post.list);

    return (
        <Route {...rest}>
            {post !== false &&
                <>
                    {
                        post
                            ? children
                            : <Redirect to='/login' />
                    }
                </>
            }
        </Route>
    );
}
