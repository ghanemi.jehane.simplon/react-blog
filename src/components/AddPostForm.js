import { Form, Button, Input } from 'antd';


export function AddPostForm({onFormSubmit}){

  function handleSubmit(value){
    onFormSubmit(value)
  }

    return(

        <div>
       
        <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={handleSubmit}
    >
      <Form.Item
        label="Title"
        name="title"
        rules={[
          {
            required: true,
            message: 'Please input your title!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Description"
        name="description"
        rules={[
          {
            required: true,
            message: 'Please input your description!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      
      <Form.Item
        label="Date"
        name="date"
        
        rules={[
          {
            required: true,
            message: 'Please input your date!',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Image"
        name="image"
        rules={[
          {
            required: true,
            message: 'Please input your image!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Auteur"
        name="auteur"
        rules={[
          {
            required: true,
            message: 'Please input your auteur!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="User id"
        name="user_id"
        rules={[
          {
            required: true,
            message: 'Please input your auteur!',
          },
        ]}
      >
        <Input />
      </Form.Item>

    

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>


        </div>
    )
}


