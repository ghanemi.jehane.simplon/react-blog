import 'antd/dist/antd.css'
import React, { useEffect } from 'react';
import { Button, Card, Col, Row } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { deletePosts, fetchPosts } from '../stores/post-slice';
import { DeleteOutlined } from '@material-ui/icons';




export default function Post() {

  const dispatch = useDispatch();
  const posts = useSelector(state => state.post.list);

  const onDelete = (id) => {
    dispatch(deletePosts(id))
    
  }

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch])

  const { Meta } = Card;

  return (
    <div>

      <Row justify="center" >
        {posts.map((item, index)=>
          <Col xs={24} md={6} key={index} span={12}>
            <Card
              post={item}
              hoverable
              style={{ width: 340 }}
              cover={<img alt="img" src={item.image} />}
            >
              <Meta title={item.title} description={item.description} />
              
              <Button onClick={() => onDelete(item.id)} type="default" danger shape="circle" htmlType="submit"><DeleteOutlined /></Button>
            </Card>
          </Col>
          
          
        )}

      </Row>


    </div>
  )
}



