import { Carousel } from "antd";


export function CarouselComp() {
    const contentStyle = {
        height: '250px',
        color: '#fff',
        lineHeight: '160px',
        textAlign: 'center',
        background: '#e51c23',
      };
    return(
        <div>
            <Carousel effect="fade">
    <div>
      <h3 style={contentStyle}>
  Welcome to NETTIPS !
      </h3>
    </div>
    <div>
      <h3 style={contentStyle}>  Find the updates of your favorite series !</h3>
    
    </div>
   
  </Carousel>
        </div>
    )
}