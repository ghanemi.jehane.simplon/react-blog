import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import { addUser } from "./user-slice";

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: '',
        registerFeedback: ''
    },

    reducers: {
        login(state, { payload }) {
            state.user = payload;
        },

        logout(state) {
            state.user = null;
            localStorage.removeItem('token');
        },

        updateLoginFeedback(state, { payload }) {
            state.loginFeedback = payload;
        },

        updateRegisterFeedback(state, { payload }) {
            state.registerFeedback = payload
        }
    }
});

export const { login, logout, updateLoginFeedback, updateRegisterFeedback } = authSlice.actions;
export default authSlice.reducer;

export const loginWithToken = () => async (dispatch) => {
    const token = localStorage.getItem("token");

    if(token){

   
    try {
        const user = await AuthService.fetchAccount(token);
        dispatch(login(user))
    } catch (error) {
        dispatch(logout())
        console.log(error)
    }

 } 
    
}
 
export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback('Login successful'))
        dispatch(login(user));
    } catch (error) {
        dispatch(updateLoginFeedback(error.response.data.error))
    }
}


export const register = (body) => async (dispatch) => {
    try {
        const { token } = await AuthService.register(body);
        localStorage.setItem('token', token);
        dispatch(updateRegisterFeedback('Registration successful'));
        dispatch(addUser(body));
        dispatch(loginWithCredentials(body));
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
    }
}