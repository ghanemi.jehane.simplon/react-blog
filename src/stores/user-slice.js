import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";

const userSlice = createSlice({
    name: 'user',
    initialState: {
        list: []
    },
    reducers: {
        setList(state, { payload }) {
            state.list = payload;
        },

        addUser(state, { payload }) {
            state.list.push(payload);
        }
    }
});

export const { setList, addUser } = userSlice.actions;
export default userSlice.reducer;


//Thunk

export const fetchUser = () => async (dispatch) => {

    try {
        const user = await AuthService.fetchUsers();
        dispatch(setList(user));
    } catch (error) {
        console.log(error)
    }
}