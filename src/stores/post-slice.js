import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { AuthService } from "../services/AuthService";

const postSlice = createSlice({
    name: 'post',
    initialState: {
        list: [],
    },

    reducers: {
        setPosts(state, { payload }) {
            state.list = payload;
        },
        addPost(state, { payload }) {
            state.list.push(payload);
        },
       
    }
})


export const { setPosts, addPost } = postSlice.actions;
export default postSlice.reducer;

export const fetchPosts = () => async (dispatch) => {

    try {
        const response = await AuthService.fetchPosts();
        dispatch(setPosts(response));
    } catch (error) {
        console.log(error)
    }

}


export const addPosts = (post) => async (dispatch) => {
    try {
        const resp = await axios.post('https://blog-projet1.herokuapp.com/api/blog/article/', post);
        dispatch(addPost(resp.data));
    } catch (error) {
        console.log(error)
    }
}

export const deletePosts = (id) => async(dispatch) => {
    try {
        await axios.delete('https://blog-projet1.herokuapp.com/api/blog/article/' + id)
        dispatch(fetchPosts())
    } catch (error) {
        console.log(error)
    }
}
