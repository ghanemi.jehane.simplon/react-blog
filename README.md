# Projet Blog front-end

[Lien de mon site](https://youthful-visvesvaraya-8b5de9.netlify.app/)
[Dépot back-end](https://gitlab.com/ghanemi.jehane.simplon/blog-projet)

## Objectif

Créer un site de blog en node/react avec authentification
## Technologies utilisées
- React
- Node JS
- Express
- MySQL
- JWT

## Fonctionnalités

- S'inscrire
- Se connecter
- Poster ou supprimer des articles (user connecté)
## Maquettes
![wireframe Blog](src/images/maquette1.png)
![wireframe Blog](src/images/maquette2.png)
![wireframe Blog](src/images/maquette3.png)

